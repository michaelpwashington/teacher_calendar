import os
import json
from flask import Flask, request, Response, send_from_directory
from flask_cors import CORS

from server.database import init_db
from models import User
from server.generate_students import *


app = Flask(__name__)
CORS(app)

init_db()
create_students()


@app.route('/lib/<path:path>')
def send_js(path):
    return send_from_directory('../lib', path)
globals()
def root_dir():  # pragma: no cover
    return os.path.abspath(os.path.dirname(__file__))

def get_file(filename):  # pragma: no cover
    try:
        src = os.path.join(root_dir(), filename)
        # Figure out how flask returns static files
        # Tried:
        # - render_template
        # - send_file
        # This should not be so non-obvious
        return open(src).read()
    except IOError as exc:
        return str(exc)

@app.route('/', methods=['GET'])
def index():
    content = get_file('../index.html')
    return Response(content, mimetype="text/html")

@app.route('/get_schedules', methods=['POST', 'GET'])
def get_schedules():
    user_json = []
    for user in User.query.all():
        user_json.append({"id": user.id, "name": user.name, "schedule": user.schedule, "price": user.price})

    response = app.response_class(
        response=json.dumps(user_json),
        status=200,
        mimetype='application/json'
    )
    return response
    # return json.dumps(user_json)

@app.route('/edit_schedules', methods=['POST'])
def edit_schedules():
    data = request.json
    print(data)
    id=data['id']
    schedule=data['schedule']
    user = User.query.get(id)
    user.schedule = json.dumps(schedule)
    db_session.commit()
    return "It worked"
                           