from sqlalchemy import Column, Integer, String
from server.database import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=False)
    schedule = Column(String(3000), unique=False)
    price = Column(Integer, unique=False)

    def __init__(self, name=None, schedule=None, price=20):
        self.name = name
        self.schedule = schedule
        self.price = price

    # def __repr__(self):
    #     return '<User %r>' % (self.name)